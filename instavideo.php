
  <style type="text/css">
/* Mobile */
@media (max-width: 767px) {
 .glyphicon-random{
    font-size: 0.9em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.6em;
  }
   #socialMedia li{
      font-size: 0.8em;
  }
   #testLogo{
    padding-top: 20px;
    width: 40% !important;
  }
  #secondRow, #thirdRow{
      height: 1000px;
 }
 .facebook-responsive {
  height: 420px !important;
  width: 252px !important;
  margin-left: auto !important;  
  margin-right: auto !important;
 }

}

/* tablets */
@media (max-width: 991px) and (min-width: 768px) {
   .glyphicon-random{
    font-size: 1.0em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.8em;
  } 
   #socialMedia li{
      font-size: 1.0em;
  }
   #testLogo{
    padding-top: 20px;
  }
  #secondRow, #thirdRow{
      height: 540px;
 }
  .facebook-responsive {
  height: 600px !important;
    width: 322px !important

 }
}

/* laptops */
@media (max-width: 1023px) and (min-width: 992px) {
   .glyphicon-random{
    font-size: 1.2em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
   #socialMedia li{
      font-size: 1.2em;
  }
   #testLogo{
    padding-top: 15px;
  }
  #secondRow, #thirdRow{
      height: 300px;
 }
}

/* desktops */
@media (min-width: 1024px) {
  .glyphicon-random{
    font-size: 1.5em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
  #socialMedia li{
      font-size: 1.6em;
  }
    #testLogo{
    padding-top: 8px;
  }
 
 #secondRow, #thirdRow{
      height: 300px;
 }
}

  .glyphicon-random{
    float: left;
  }
  .glyphicon-facetime-video{
    float: left;
      padding-top: 10px;

  }
  *{
    word-wrap: break-word;
    font-family: "Futura",Times, serif; 
    }
    body{
        background-color: #f2f2f2;
  }

  .glyphicon-random:hover, #socialMedia li:hover, #sideBar li:hover{
    color: #f2f2f2;
  }

  .glyphicon-search{
    margin-right: 10px;
  }
  #glyph{
    float: left;
    margin-right: 0px;
    margin-left: 10px;
    margin-top: 10px;
    width: 5%
  }

  #menuBar{
  background-color: #b8e0f6;
  height: 100px;
  }

  #menuBar p{
    float: left;
  }

  #testLogo{
    width: 23%;
    float: left;
  }

  #socialMedia{
    position: relative;
    width: 40%;
    float: right;
    margin-top: 30px;
    padding-left: 40px;

  }

  #socialMedia li{
    list-style-type: none;
    float: left;
    padding-right: 10px;
  }

  #sideBar{
    float: left;
    width: 8%
  }

  #sideBar li{
    list-style-type: none;
    font-size: 3.5em;
  }

  .fa{
    font-size: 1.5em;
    margin-left: 8px;
  }

  #sideBar ul{
      background-color: #b8e0f6;
    width: 7.0em;
    height: 15em;
   }

   #promotedVid{
    float: left;
    margin-left: 20px;
    margin-top: 20px;
   }

   #promotedVid img{
    box-shadow: 5px 5px 10px #878787;
   }

   .promoted{
    padding-left: 10px;
    padding-right: 20px;
    float: left;
    opacity: 0.7;
   }

   .promoted:hover{
    opacity: 1.0;
   }

   .promoted h2{
    padding-left: 8px;
   }

   .promoted h4{
    padding-left: 10px;
   }

   .headerVideos{
    font-size: 1.9em;
   }

   #secondRow, #thirdRow{
    margin-top: 10px;
    background-color: #b8e0f6 !important;
    border: solid #f2f2f2 10px;
   }

   #secondRow img{
    float: left;
    margin-right: 70px;
        box-shadow: 0px 0px 7px 2px #878787;

   }

   #secondRow p, #suggested p{
  clear: both;
   }

   .glyphy{
    float: left;
   }
   

  .video{
  	padding: 60px 0px 0px 0px;
  }
  


.facebook-responsive {
    overflow:hidden;
    margin-top: 40px;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.facebook-responsive iframe {
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}
  
  .adPanel{
  	margin-top: 15px;
  	margin-left: 10px;
  }

  .rightPanel{
  	margin: 30px 0px 50px 0px;
  	padding-left: 40px;
  	font-size: 1.6em;
  }
  	.stars {
  		padding: 70px 0 0 0;
  	}

   .stars li{
   	display: inline;
   }

   .glyphicon-star:hover, .glyphicon-star-empty:hover{
   	color: red;
   }

   .vidCaption{
   	margin-bottom: 10px;
   }

   .vidSource{
   	padding-top:30px;
   	padding-left: 0 !important;
   }

   .smallMedHidden{
    padding: 50px 0 0 0;
   }

  </style>
  
  
 <div class="container">

 <div class="smallMedHidden hidden-lg hidden-md col-xs-12 col-sm-12">
   <p><span class="vidCaption">Caption for video</span> <br />
      <span class="vidSource">@Someone via Twitter</span><br /><br /> 
      <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%

      </p> 
 </div>

 	<div class="facebook-responsive row video col-lg-6 col-md-6 col-sm-12 col-xs-12">	
 		<blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BQqQuJ7lf0H/'" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by The Shade Room (@theshaderoom)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-02-18T16:52:46+00:00">Feb 18, 2017 at 8:52am PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
 	</div>

 	<div class="row rightPanel col-lg-3 col-md-3 hidden-sm hidden-xs">
 		
 		
 			<p><span class="vidCaption">Caption for video</span> <br />
 			<span class="vidSource">@Someone via Twitter</span><br /><br /> 
 			<span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%

 			</p> 
    <div style="clear:both;"></div>

 			<p>
 			<ul class="stars">
 				<li class="glyphicon glyphicon-star"></li>
 				<li class="glyphicon glyphicon-star"></li>
 				<li class="glyphicon glyphicon-star"></li>
 				<li class="glyphicon glyphicon-star"></li>
 				<li class="glyphicon glyphicon-star-empty"></li>
 			</ul>
 			</p><br />
 		</div>
 		<!--
		STAR RATINGS GO HERE
 		-->
 	</div>

 	<div class="row adPanel col-lg-12 col-md-12 col-sm-12 col-xs-12">
 		<img class="img-responsive" src="adbanner.png" alt="adBanner">
 		
 	</div>
 	<div id="secondRow" class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!--
        POP CULTURE VIDEOS ROW
            -->
            <h2 class="headerVideos"><span class="glyphicon glyphicon-chevron-left"></span>Suggested Videos<span class="glyphicon glyphicon-chevron-right"></h2>
            <div class="">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid1.jpg" alt="vid1">
                 <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>

              </div>
              
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid2.jpg" alt="vid2">
                <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>    
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid3.jpg" alt="vid3">
                <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>                 
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid4.jpg" alt="vid4" >
                 <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>

            </div>
      </div>

 </div>
</body>
</html>