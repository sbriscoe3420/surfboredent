
  <style type="text/css">
/* Mobile */
@media (max-width: 767px) {
 .glyphicon-random{
    font-size: 0.9em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.6em;
  }
   #socialMedia li{
      font-size: 0.8em;
  }
   #testLogo{
    padding-top: 20px;
    width: 40% !important;
  }
  #secondRow, #thirdRow{
      height: 1000px;
 }
 .facebook-responsive {
  height: 420px !important;
  width: 252px !important;
  margin-left: auto !important;  
  margin-right: auto !important;
 }

}

/* tablets */
@media (max-width: 991px) and (min-width: 768px) {
   .glyphicon-random{
    font-size: 1.0em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.8em;
  } 
   #socialMedia li{
      font-size: 1.0em;
  }
   #testLogo{
    padding-top: 20px;
  }
  #secondRow, #thirdRow{
      height: 540px;
 }
  .facebook-responsive {
  height: 600px !important;
    width: 322px !important

 }
}

/* laptops */
@media (max-width: 1023px) and (min-width: 992px) {
   .glyphicon-random{
    font-size: 1.2em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
   #socialMedia li{
      font-size: 1.2em;
  }
   #testLogo{
    padding-top: 15px;
  }
  #secondRow, #thirdRow{
      height: 300px;
 }
}

/* desktops */
@media (min-width: 1024px) {
  .glyphicon-random{
    font-size: 1.5em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
  #socialMedia li{
      font-size: 1.6em;
  }
    #testLogo{
    padding-top: 8px;
  }
 
 #secondRow, #thirdRow{
      height: 300px;
 }
}

body{
        background-color: #f2f2f2;
  }

  .glyphicon-random{
    float: left;
  }
  .glyphicon-facetime-video{
    float: left;
      padding-top: 10px;

  }
  *{
    word-wrap: break-word;
    font-family: "Futura",Times, serif;

  }

  .glyphicon-random:hover, #socialMedia li:hover, #sideBar li:hover{
    color: #f2f2f2;
  }

  .glyphicon-search{
    margin-right: 10px;
  }
  #glyph{
    float: left;
    margin-right: 0px;
    margin-left: 10px;
    margin-top: 10px;
    width: 5%
  }

  #menuBar{
  background-color: #b8e0f6;
  height: 100px;
  }

  #menuBar p{
    float: left;
  }

  #testLogo{
    width: 23%;
    float: left;
  }

  #socialMedia{
    position: relative;
    width: 40%;
    float: right;
    margin-top: 30px;
    padding-left: 40px;

  }

  #socialMedia li{
    list-style-type: none;
    float: left;
    padding-right: 10px;
  }

  #sideBar{
    float: left;
    width: 8%
  }

  #sideBar li{
    list-style-type: none;
    font-size: 3.5em;
  }

  .fa{
    font-size: 1.5em;
    margin-left: 8px;
  }

  #sideBar ul{
      background-color: #b8e0f6;
    width: 7.0em;
    height: 15em;
   }

   #promotedVid{
    float: left;
    margin-left: 20px;
    margin-top: 20px;
   }

   #promotedVid img{
    box-shadow: 5px 5px 10px #878787;
   }

   .promoted{
    padding-left: 10px;
    padding-right: 20px;
    float: left;
    opacity: 0.7;
   }

   .promoted:hover{
    opacity: 1.0;
   }

   .promoted h2{
    padding-left: 8px;
   }

   .promoted h4{
    padding-left: 10px;
   }

   .headerVideos{
    font-size: 1.9em;
   }

   #secondRow, #thirdRow{
    margin-top: 10px;
    background-color: #b8e0f6 !important;
    border: solid #f2f2f2 10px;
   }

   #secondRow img{
    float: left;
    margin-right: 70px;
        box-shadow: 0px 0px 7px 2px #878787;

   }

   #secondRow p, #suggested p{
  clear: both;
   }

   .glyphy{
    float: left;
   }
   

  .video{
  	padding: 60px 0px 0px 0px;
  }
  


.facebook-responsive {
    overflow:hidden;
    margin-top: 40px;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.facebook-responsive iframe {
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}
  
  .adPanel{
  	margin-top: 15px;
  	margin-left: 10px;
  }

  .rightPanel{
  	margin: 30px 0px 50px 0px;
  	padding-left: 40px;
  	font-size: 1.6em;
  }
  	.stars {
  		padding: 70px 0 0 0;
  	}

   .stars li{
   	display: inline;
   }

   .glyphicon-star:hover, .glyphicon-star-empty:hover{
   	color: red;
   }

   .vidCaption{
   	margin-bottom: 10px;
   }

   .vidSource{
   	padding-top:30px;
   	padding-left: 0 !important;
   }

   .smallMedHidden{
    padding: 50px 0 0 0;
   }

  </style>
  
 <div class="container">

 <div class="smallMedHidden hidden-lg hidden-md col-xs-12 col-sm-12">
   <p><span class="vidCaption"></span> <br />
      <span class="vidSource"></span><br /><br /> 
      </p> 
 </div>

 	<div class="facebook-responsive row video col-lg-6 col-md-6 col-sm-12 col-xs-12">	
 		
 		
 	</div>

 	<div class="row rightPanel col-lg-3 col-md-3 hidden-sm hidden-xs">
 		
 		
 			<p><span class="vidCaption"></span> <br />
 			<span class="vidSource"></span><br /><br /> 

 			</p> 
    <div style="clear:both;"></div>

 			<p>
 	
 			</p><br />
 		</div>
 		<!--
		STAR RATINGS GO HERE
 		-->
 	</div>

 	<div class="row adPanel col-lg-12 col-md-12 col-sm-12 col-xs-12">
 		<img class="img-responsive" src="adbanner.png" alt="adBanner">
 		
 	</div>
 	<div id="secondRow" class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!--
        POP CULTURE VIDEOS ROW
            -->
            <h2 class="headerVideos"><span class="glyphicon glyphicon-chevron-left"></span>Suggested Videos<span class="glyphicon glyphicon-chevron-right"></h2>
            <div class="">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid1.jpg" alt="vid1">
                 <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>

              </div>
              
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid2.jpg" alt="vid2">
                <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>    
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid3.jpg" alt="vid3">
                <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>                 
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                 <img src="vid4.jpg" alt="vid4" >
                 <p>Caption for pictures <br />                 
                  <span class="glyphicon glyphicon-eye-open"></span> 1.2k  <br />89%
                 </p>

            </div>
      </div>

 </div>
 <script src="https://www.gstatic.com/firebasejs/3.6.9/firebase.js"></script>
 <script>
   
   $(document).ready(function(){
// Initialize Firebase
  var config = {
    apiKey: "AIzaSyDce9HerQ0hJG4f8A9cVFtFr2B7g0rVNH4",
    authDomain: "surfbored-d276a.firebaseapp.com",
    databaseURL: "https://surfbored-d276a.firebaseio.com",
    storageBucket: "surfbored-d276a.appspot.com",
    messagingSenderId: "582525741382"
  };//end of config

  firebase.initializeApp(config);
            $(".togPara").hide();

$(".glyphicon-menu-hamburger").click(function(){
          $(".togPara").toggle();
          $(".bottom").toggleClass("toggleDiv");
          $("#menuBar").toggleClass("borderTog");
          $(".glyphicon-menu-hamburger").toggleClass("toggleMenu"); 
      });
  var isClickedUpdate = false;
  function appendVideo(urlFirebase){
    var catRef = firebase.database().ref(urlFirebase);
    catRef.on('value', snap => {

       var thumbnail =       snap.child("imageURL").val();
            var thumbnailTitle =  snap.child("title").val();
            var source =          snap.child("author").val();
            var viaSrc =          snap.child("via").val();
            var category =        snap.child("category").val();
            var vidId =        snap.child("id").val();
            var vUrl =        snap.child("url").val();

 
      $(".facebook-responsive").append('<iframe src="'+ vUrl +'" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>' 
                  );//videoDiv append END

      $(".vidCaption").text(thumbnailTitle);
      $(".vidSource").text(source);

      $(".glyphicon-menu-hamburger").click(function(){
    $(".togPara").toggle();
          $(".bottom").toggleClass("toggleDiv");
          $("#menuBar").toggleClass("borderTog");
          $(".glyphicon-menu-hamburger").toggleClass("toggleMenu"); 
      });

     

    });//caton end
     



  }

var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var passedURL = getQueryString('vid');
if (passedURL.indexOf('/') == -1) {
    $(".facebook-responsive").append('<div class="alert alert-danger"><p>This video is unavailable. </p></div>');
    //if the url doesn't not have a / in it, alert.
}else{
    appendVideo(passedURL);

}



});//end document ready
 </script>
</body>
</html>