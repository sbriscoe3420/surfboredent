<?php include('header.php');?>

  <style type="text/css">
/* Mobile */
@media (max-width: 767px) {
 .glyphicon-random{
    font-size: 0.9em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.6em;
  }
   #socialMedia li{
      font-size: 0.8em;
  }
   #testLogo{
    padding-top: 20px;
    width: 40% !important;
  }
  #secondRow, #thirdRow{
      height: 1000px;
 }
}

/* tablets */
@media (max-width: 991px) and (min-width: 768px) {
   .glyphicon-random{
    font-size: 1.0em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.8em;
  } 
   #socialMedia li{
      font-size: 1.0em;
  }
   #testLogo{
    padding-top: 20px;
  }
  #secondRow, #thirdRow{
      height: 540px;
 }
}

/* laptops */
@media (max-width: 1023px) and (min-width: 992px) {
   .glyphicon-random{
    font-size: 1.2em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
   #socialMedia li{
      font-size: 1.2em;
  }
   #testLogo{
    padding-top: 15px;
  }
  #secondRow, #thirdRow{
      height: 300px;
 }
 .sideBarDiv li{
  font-size: 0.9em;
 }
}

/* desktops */
@media (min-width: 1024px) {
  .glyphicon-random{
    font-size: 1.5em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
  #socialMedia li{
      font-size: 1.6em;
  }
    #testLogo{
    padding-top: 8px;
  }
 
 #secondRow, #thirdRow{
      height: 300px;
 }
 .sideBarDiv li{
  font-size: 0.9em;
 }
 
}

  .glyphicon-random{
    float: left;
  }
  .glyphicon-facetime-video{
    float: left;
      padding-top: 10px;

  }
  *{
    word-wrap: break-word;
    font-family: "Futura",Times, serif;

  }


  body{
        background-color: #f2f2f2;
        
  }

  p{
    overflow: hidden;
    text-overflow: ellipsis; 
    white-space: nowrap; 
  }
  #socialMedia li:hover, #sideBar li:hover, .glyphicon-random:hover{
    color: #f2f2f2;
  }

  .glyphicon-search{
    margin-right: 10px;
  }
  #glyph{
    float: left;
    margin-right: 0px;
    margin-left: 10px;
    margin-top: 10px;
    width: 5%
  }

  #menuBar{
  background-color: #b8e0f6;
  height: 100px;
  }

  #menuBar p{
    float: left;
  }

  #testLogo{
    width: 23%;
    float: left;
  }

  #socialMedia{
    position: relative;
    width: 40%;
    float: right;
    margin-top: 30px;
    padding-left: 40px;

  }

  #socialMedia li{
    list-style-type: none;
    float: left;
    padding-right: 10px;
  }


  .sideCatHeader{
    font-size: 1.3em;
    margin: 2px 0 0 5px;
  }

  #sideBar ul{
      background-color: #b8e0f6;
    width: 7.0em;
    height: 15em;
   }

  

   li a{
    display: block;
   }

   #secondRow p{
  clear: both;
   }

   .glyphy{
    float: left;
   }
   
   .sideBarDiv ul{
    list-style: none;
    margin: 10px 0 0 0;
    padding: 0 0 0 0;
    font-size: 1.3em;
    background-color: #b8e0f6; 
   }

   .sideBarDiv li{
        border-left: 4px solid;
        margin-bottom: 3px;
   }
   .sideBarDiv li{
    padding: 4px 4px 4px 4px;
   }

   .sideBarDiv li:hover{
    color: black;
    font-weight: bold;
   }

   .sideBarDiv li:active{
    font-weight: bold;
   }

   .videoDiv h3{
    padding: 0 0 20px 20px;
   }

   .vid{
    padding: 0 0 35px 0;
      opacity: 0.8;

   }

   .vid img{
    box-shadow: 5px 5px 10px #878787;
    margin: 0 0 7px 0;
   }

   .vid:hover{
    opacity: 1;
   }

   .vid p{
    padding: 0 25px 0 0;
   }

   .listClick{
    cursor: pointer;
   }



  </style>
  
  

</div>

<div class="bodyDiv"> 
    <div class="sideBarDiv col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <p class="sideCatHeader">Categories </p>
        <!-- THIS DIV IS THE SIDEBAR
        -->
        <ul id="sideBarCat">                     
        </ul>
    </div>

    <div class="videoDiv col-lg-10 col-md-10 col-sm-12 col-xs-12">
      <!-- THIS DIV IS FOR THE VIDEOS
      -->

    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/3.6.9/firebase.js"></script>
<script>
 

$(document).ready(function(){
// Initialize Firebase
  var config = {
    apiKey: "AIzaSyDce9HerQ0hJG4f8A9cVFtFr2B7g0rVNH4",
    authDomain: "surfbored-d276a.firebaseapp.com",
    databaseURL: "https://surfbored-d276a.firebaseio.com",
    storageBucket: "surfbored-d276a.appspot.com",
    messagingSenderId: "582525741382"
  };
            $(".togPara").hide();


$(".glyphicon-menu-hamburger").click(function(){
          $(".togPara").toggle();
          $(".bottom").toggleClass("toggleDiv");
          $("#menuBar").toggleClass("borderTog");
          $(".glyphicon-menu-hamburger").toggleClass("toggleMenu"); 
      });

  var array = [];

  function runQuery(query){
  var catRef = firebase.database().ref(query);
    catRef.orderByChild('timestamp').limitToFirst(50).on('child_added', snap => {
      var thumbnail =       snap.child("imageURL").val();
      var thumbnailTitle =  snap.child("title").val();
      var source =          snap.child("author").val();
      var viaSrc =          snap.child("via").val();
      var category =        snap.child("category").val();
      var vidId =        snap.child("id").val();

      $(".videoDiv").append( '<div id="'+ category + '/' + vidId +'"  class="vid mainVid col-lg-3 col-md-3 col-sm-6 col-xs-12 ' + category+ 'div"> <a href="video.php?vid='+ category + "/" + vidId +
               '"><img width="250px" height="158px"  alt="videoThumbnail" class="img-responsive" data-toggle="tooltip" title="' + thumbnailTitle + '"  src="'+ thumbnail +' "> </a>'+
                '<p> <a href="video.php?vid='+ category + "/" + vidId +'">'+ thumbnailTitle + '</a><br />' +
                source + "</p></div>"
                  );//videoDiv append END
    });                 $('[data-toggle="tooltip"]').tooltip();


  }//end of func runQuery

  function replaceUnder(){
  $('h2').each(function() {
    var $this = $(this);

    $this.text($this.text().replace(/_/g, ' '));
});

  $('.listClick').each(function() {
    var $this = $(this);

    $this.text($this.text().replace(/_/g, ' '));
});
}//end of func replaceUnder

  function appendH2(){
     $(".Beautyh2").text("Beauty and Fashion");
              $(".Gossiph2").text("Gossip and Shade");
              $(".Comich2").text("Comic Books");
              $(".Moviesh2").text("Movies and Television");
              $(".Techh2").text("Tech and Science");


              //edit keyword to full name
  }

  function appendli(){
            $(".Beautyli").text("Beauty and Fashion");
            $(".Gossipli").text("Gossip and Shade");
            $(".Comicli").text("Comic Books");
            $(".Moviesli").text("Movies and Television");
                $(".Techli").text("Tech and Science");

  }

  function pageLoadRandomCat(){
    var rand = array[Math.floor(Math.random() * array.length)];
    $(".videoDiv").append('<h2 class =' + rand + 'h2>' + rand + '</h2>' );
    replaceUnder();
    runQuery(rand);
    appendH2();
             

  }
  function firebaseVid(v){
    if (v != "undefined"){
    window.location = "video.php?vid=" + v;
  }else{
    window.location ="index.php";
  }
}

  firebase.initializeApp(config);

var promotedRef = firebase.database().ref();
    promotedRef.on('value', snap => {
        var key = String(snap.val());
        var counter = snap.numChildren();
         var obj = snap.val();

         for ( property in obj ) {
           if (property != "promoted"){
                if(property != "Views"){

           array.push(property);

           $("#sideBarCat").append('<li class="listClick '+ property +'li" id="' + property + '">' + property + '</li>');
              }
            }
            appendli();

          }

          if (array.length != 0){
            pageLoadRandomCat();
          }


          $(".listClick").click(function(){
              $(".videoDiv").empty();
              var queryKey = $(this).attr('id');

              $(".videoDiv").append('<h2 class="' + queryKey + 'h2">' + queryKey + '</h2>' );
              replaceUnder();
              runQuery(queryKey);
              //append videos w the keyword

              appendH2();

          });//listclick function

          $(".mainVid").click(function(){

            var idOfObj= $(this).attr("id");
            firebaseVid(idOfObj);
          });

      });//promotedRef.on value END

          $(".glyphicon-random").click(function(){
            

          });//glyph random func
});//document.ready END

  


</script>
</body>
</html>

