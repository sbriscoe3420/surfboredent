<?php include('header.php');?>

  <style type="text/css">
/* Mobile */
@media (max-width: 767px) {
 .glyphicon-random{
    font-size: 0.9em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.6em;
  }
   #socialMedia li{
      font-size: 0.8em;
  }
   #testLogo{
    padding-top: 20px;
    width: 40% !important;
  }
  #secondRow, #thirdRow{
      height: 1000px;
 }
}

/* tablets */
@media (max-width: 991px) and (min-width: 768px) {
   .glyphicon-random{
    font-size: 1.0em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.8em;
  } 
   #socialMedia li{
      font-size: 1.0em;
  }
   #testLogo{
    padding-top: 20px;
  }
  #secondRow, #thirdRow{
      height: 540px;
 }
}

/* laptops */
@media (max-width: 1023px) and (min-width: 992px) {
   .glyphicon-random{
    font-size: 1.2em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
   #socialMedia li{
      font-size: 1.2em;
  }
   #testLogo{
    padding-top: 15px;
  }
  #secondRow, #thirdRow{
      height: 300px;
 }
 .sideBarDiv li{
  font-size: 0.9em;
 }
}

/* desktops */
@media (min-width: 1024px) {
  .glyphicon-random{
    font-size: 1.5em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
  #socialMedia li{
      font-size: 1.6em;
  }
    #testLogo{
    padding-top: 8px;
  }
 
 #secondRow, #thirdRow{
      height: 300px;
 }
 .sideBarDiv li{
  font-size: 0.9em;
 }
 
}

  .glyphicon-random{
    float: left;
  }
  .glyphicon-facetime-video{
    float: left;
      padding-top: 10px;

  }
  *{
    word-wrap: break-word;
    font-family: "Futura",Times, serif;

  }


  body{
        background-color: #f2f2f2;
        
  }

  p{
    overflow: hidden;
    text-overflow: ellipsis; 
    white-space: nowrap; 
  }
  #socialMedia li:hover, #sideBar li:hover, .glyphicon-random:hover{
    color: #f2f2f2;
  }

  .glyphicon-search{
    margin-right: 10px;
  }
  #glyph{
    float: left;
    margin-right: 0px;
    margin-left: 10px;
    margin-top: 10px;
    width: 5%
  }

  #menuBar{
  background-color: #b8e0f6;
  height: 100px;
  }

  #menuBar p{
    float: left;
  }

  #testLogo{
    width: 23%;
    float: left;
  }

  #socialMedia{
    position: relative;
    width: 40%;
    float: right;
    margin-top: 30px;
    padding-left: 40px;

  }

  #socialMedia li{
    list-style-type: none;
    float: left;
    padding-right: 10px;
  }


  .sideCatHeader{
    font-size: 1.3em;
    margin: 2px 0 0 5px;
  }

  #sideBar ul{
      background-color: #b8e0f6;
    width: 7.0em;
    height: 15em;
   }

  

   li a{
    display: block;
   }

   #secondRow p{
  clear: both;
   }

   .glyphy{
    float: left;
   }
   
   .sideBarDiv ul{
    list-style: none;
    margin: 10px 0 0 0;
    padding: 0 0 0 0;
    font-size: 1.3em;
    background-color: #b8e0f6; 
   }

   .sideBarDiv li{
        border-left: 4px solid;
        margin-bottom: 3px;
   }
   .sideBarDiv li{
    padding: 4px 4px 4px 4px;
   }

   .sideBarDiv li:hover{
    color: black;
    font-weight: bold;
   }

   .sideBarDiv li:active{
    font-weight: bold;
   }

   .urlDiv{
    margin: 30px 0px 0px 0;
   }

   input{
    width: 80%;
   }

   button{
    margin: 20px 0 30px 0;
   }
   .video{
    padding: 30px 0px 0px 0px;
  }
  
.video-frame {
  position:relative;
  padding-bottom:56.25%;
  padding-top:30px;
  height:0;
  overflow:hidden;
}

.video-frame iframe, .video-container object, .video-container embed {
  position:absolute;
  top:0;
  left:0;
  width:100%;
  height:100%;
}
  
  .adPanel{
    margin-top: 15px;
    margin-left: 10px;
  }

  .rightPanel{
    margin: 30px 0px 50px 0px;
    padding-left: 40px;
    font-size: 1.3em;
  }
  

   .stars li{
    display: inline;
   }


   .glyphicon-star:hover, .glyphicon-star-empty:hover{
    color: red;
   }

   .vidCaption{
    margin-bottom: 10px;
    width: 100% !important;
   }

   .vidSource{
    padding-top:30px;
    padding-left: 0 !important;
    color: #8c8c8c;
   }
   .video-frame{
    margin: 0px 0 0 20px;
   }

   .smallMedHidden{
    padding: 50px 0 0 0;
   }
   .urlcopy{
    padding: 0 20px 0px 0;
    margin: 0 20px 20px 0;
   }
  


.facebook-responsive {
    overflow:hidden;
    margin-top: 40px;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.facebook-responsive iframe {
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}



  </style>
  
  

</div>

<div class="bodyDiv"> 
    <div class="urlDiv col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <label for="url">URL:</label>
                <input type="text" required="" placeholder="Input video URL" value="" name="url" class="txt" id="vidURL"><br /><br /> <br />
      <button>Submit</button>
    </div>


    <div class="youtube col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <p>Youtube</p>
    </div>


    <div class="facebook col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <p>Facebook</p>
    </div>


    <div class="twitter col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <p>Twitter</p>
    </div>


    <div class="instagram col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <p>Instagram</p>
    </div>



</div>
<script src="https://www.gstatic.com/firebasejs/3.6.9/firebase.js"></script>
<script>
 

$(document).ready(function(){
// Initialize Firebase
  var config = {
    apiKey: "AIzaSyDce9HerQ0hJG4f8A9cVFtFr2B7g0rVNH4",
    authDomain: "surfbored-d276a.firebaseapp.com",
    databaseURL: "https://surfbored-d276a.firebaseio.com",
    storageBucket: "surfbored-d276a.appspot.com",
    messagingSenderId: "582525741382"
  };
            $(".togPara").hide();


$(".glyphicon-menu-hamburger").click(function(){
          $(".togPara").toggle();
          $(".bottom").toggleClass("toggleDiv");
          $("#menuBar").toggleClass("borderTog");
          $(".glyphicon-menu-hamburger").toggleClass("toggleMenu"); 
      });

$("button").click(function(){
   var url = $("#vidURL").val();

   var vUrl = url.replace("watch?v=", "embed/");


          
              $(".youtube").append('<iframe width="640" height="360" class="original" src=' + vUrl + ' frameborder="0" allowfullscreen></iframe>' 
                  );//videoDiv append END
            

              $(".twitter").append(vUrl 
                    );//videoDiv append END            

         
              $(".facebook").append(vUrl 
                    );//videoDiv append END
    
              $(".instagram").append(vUrl 
                    );//videoDiv append END
          
});//button click function


         

  
});//document.ready END

  


</script>
</body>
</html>

