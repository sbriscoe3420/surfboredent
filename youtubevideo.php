
<style type="text/css">
/* Mobile */
@media (max-width: 767px) {
 .glyphicon-random{
    font-size: 0.9em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.6em;
  }
   #socialMedia li{
      font-size: 0.8em;
  }
   #testLogo{
    padding-top: 20px;
    width: 40% !important;
  }
  #secondRow, #thirdRow{
      height: 1000px;
 }
}

/* tablets */
@media (max-width: 991px) and (min-width: 768px) {
   .glyphicon-random{
    font-size: 1.0em;
  }  
  .glyphicon-facetime-video{
    font-size: 1.8em;
  } 
   #socialMedia li{
      font-size: 1.0em;
  }
   #testLogo{
    padding-top: 20px;
  }
  #secondRow, #thirdRow{
      height: 540px;
 }
}

/* laptops */
@media (max-width: 1023px) and (min-width: 992px) {
   .glyphicon-random{
    font-size: 1.2em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
   #socialMedia li{
      font-size: 1.2em;
  }
   #testLogo{
    padding-top: 15px;
  }
  #secondRow, #thirdRow{
      height: 300px;
 }
}

/* desktops */
@media (min-width: 1024px) {
  .glyphicon-random{
    font-size: 1.5em;
  }  
  .glyphicon-facetime-video{
    font-size: 2.0em;
  }
  #socialMedia li{
      font-size: 1.6em;
  }
    #testLogo{
    padding-top: 8px;
  }
 
 #secondRow, #thirdRow{
      height: 300px;
 }
}

  .glyphicon-random{
    float: left;
  }
  .glyphicon-facetime-video{
    float: left;
      padding-top: 10px;

  }
  *{
    word-wrap: break-word;
    font-family: "Futura",Times, serif;

  }
  body{
        background-color: #f2f2f2;
  }

  .glyphicon-random:hover, #socialMedia li:hover, #sideBar li:hover{
    color: #f2f2f2;
  }

  .glyphicon-search{
    margin-right: 10px;
  }
  #glyph{
    float: left;
    margin-right: 0px;
    margin-left: 10px;
    margin-top: 10px;
    width: 5%
  }

  #menuBar{
  background-color: #b8e0f6;
  height: 100px;
  }

  #menuBar p{
    float: left;
  }

  #testLogo{
    width: 23%;
    float: left;
  }

  #socialMedia{
    position: relative;
    width: 40%;
    float: right;
    margin-top: 30px;
    padding-left: 40px;

  }

  #socialMedia li{
    list-style-type: none;
    float: left;
    padding-right: 10px;
  }

  #sideBar{
    float: left;
    width: 8%
  }

  #sideBar li{
    list-style-type: none;
    font-size: 3.5em;
  }

  .fa{
    font-size: 1.5em;
    margin-left: 8px;
  }

  #sideBar ul{
      background-color: #b8e0f6;
    width: 7.0em;
    height: 15em;
   }

   #promotedVid{
    float: left;
    margin-left: 20px;
    margin-top: 20px;
   }

   #promotedVid img{
    box-shadow: 5px 5px 10px #878787;
   }

   .promoted{
    padding-left: 10px;
    padding-right: 20px;
    float: left;
    opacity: 0.7;
   }

   .promoted:hover{
    opacity: 1.0;
   }

   .promoted h2{
    padding-left: 8px;
   }

   .promoted h4{
    padding-left: 10px;
   }

   .headerVideos{
    font-size: 1.9em;
   }

   #secondRow, #thirdRow{
    margin-top: 10px;
    background-color: #b8e0f6 !important;
    border: solid #f2f2f2 10px;
   }

   #secondRow img{
    float: left;
    margin-right: 70px;
        box-shadow: 0px 0px 7px 2px #878787;

   }

   #secondRow p, #suggested p{
  clear: both;
   }

   .glyphy{
    float: left;
   }
   

  .video{
  	padding: 60px 0px 0px 0px;
  }
  
.video-frame {
	position:relative;
	padding-bottom:56.25%;
	padding-top:30px;
	height:0;
	overflow:hidden;
}

.video-frame iframe, .video-container object, .video-container embed {
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
}
  
  .adPanel{
  	margin-top: 15px;
  	margin-left: 10px;
  }

  .rightPanel{
  	margin: 30px 0px 50px 0px;
  	padding-left: 40px;
  	font-size: 1.6em;
  }
  	.stars {
  		padding: 70px 0 0 0;
  	}

   .stars li{
   	display: inline;
   }

   .glyphicon-star:hover, .glyphicon-star-empty:hover{
   	color: red;
   }

   .vidCaption{
   	margin-bottom: 10px;
   }

   .vidSource{
   	padding-top:30px;
   	padding-left: 0 !important;
   }

   .smallMedHidden{
    padding: 50px 0 0 0;
   }
   .urlcopy{
    padding: 0 20px 0px 0;
    margin: 0 20px 20px 0;
   }
  </style>
 

 	
 <script>
 $(document).ready(function(){

  
 </script>
</body>
</html>