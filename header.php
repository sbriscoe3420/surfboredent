
<!DOCTYPE html>
<html lang="en">
<head>
  <title>surfBOREDent - Find viral videos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!--SLICK CAROUSEL CSS-->
<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
  <!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</head>
<body>


<div class="mainBody"> 

<div id="menuBar" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div id="glyph">
    <span class="glyphicon glyphicon-facetime-video"></span> <br /> <br />
    <span class="glyphicon glyphicon-random" title="Click to shuffle videos"></span>
        <div style="clear:both;"></div>

  </div>
  <a href="index.php"><img src="surfboredent.jpg" id="testLogo" alt="logo"></a>

  <div id="socialMedia">
  <span class="glyphicon glyphicon-menu-hamburger hidden-lg hidden-md"></span>
  <ul class="hidden-sm hidden-xs">
  
      <li class="underlineBorder"> <button type="button" class="btn" data-toggle="modal" data-target="#modal-1"><span class="glyphicon glyphicon-search"></span></button>


      </li>
      <a href="categories.php" style="color: black;"><li class="underlineBorder"> <span class="glyphicon glyphicon-arrow-up"></span>Categories</li></a>
      <div class="hidden-xs hidden-sm">
        <li><i class="fa fa-facebook-official"></i></li>
        <li><i class="fa fa-instagram"></i></li>
        <li><i class="fa fa-twitter-square"></i> </li>
        </div>
  </ul>
    <div style="clear:both;"></div>

  </div>

        <div style="clear:both;"></div>
    </div>

<div class="bottom hidden-md hidden-lg">
      <a href="categories.php"><p class="togPara">Categories</p></a>
      <a><p class="togPara">Search</p></a>
      <a href="#"><p class="togPara">Follow us on Twitter</p></a>
      <a href="#"><p class="togPara">Follow us on Instagram</p></a>
      <a href="#"><p class="togPara">Like us on Facebook</p></a>
</div>

